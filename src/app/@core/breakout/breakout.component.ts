import { Component, OnInit } from '@angular/core';
import { SteupServiceService } from 'src/app/services/steup-service.service';

@Component({
  selector: 'app-breakout',
  templateUrl: './breakout.component.html',
  styleUrls: ['./breakout.component.scss']
})
export class BreakoutComponent implements OnInit {

  constructor(private _analytics: SteupServiceService) { }

  ngOnInit(): void {
  }

  stepup(action) {
    this._analytics.stepUpAnalytics(action);
  }
}
