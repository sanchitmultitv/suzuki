import { Component, OnInit } from '@angular/core';
import { CometChat } from "@cometchat-pro/chat";

@Component({
  selector: 'app-comet',
  templateUrl: './comet.component.html',
  styleUrls: ['./comet.component.scss']
})
export class CometComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {

    const appID = "3266456fe0820d1";
    const region = "us";
    const appSetting = new CometChat.AppSettingsBuilder()
      .subscribePresenceForAllUsers()
      .setRegion(region)
      .build();
    CometChat.init(appID, appSetting).then(
      () => {
        console.log("Initialization completed successfully");
        // You can now call login function.
      },
      (error) => {
        console.log("Initialization failed with error:", error);
        // Check the reason for error and take appropriate action.
      }
    );

    const authKey = "c820b52ff197a5639184d28a39b2496fc7189551";
    const uid = "divakar123";

    CometChat.login(uid, authKey).then(
      (user) => {
        console.log("Login Successful:", { user });
      },
      (error) => {
        console.log("Login failed with exception:", { error });
      }
    );
  }

}
