import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Eventhall3RoutingModule } from './eventhall3-routing.module';
import { Eventhall3Component } from './eventhall3.component';


@NgModule({
  declarations: [Eventhall3Component],
  imports: [
    CommonModule,
    Eventhall3RoutingModule
  ]
})
export class Eventhall3Module { }
