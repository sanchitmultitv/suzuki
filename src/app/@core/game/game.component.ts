import { Component, OnInit } from '@angular/core';
import { SteupServiceService } from 'src/app/services/steup-service.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {

  constructor(private _analytics: SteupServiceService) { }

  ngOnInit(): void {
  }

  stepup(action) {
    this._analytics.stepUpAnalytics(action);
  }

}
