import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoungeChatComponent } from './lounge-chat/lounge-chat.component';
import { NetworkingLoungeComponent } from './networking-lounge.component';


const routes: Routes = [
  {
    path:'', 
    component: NetworkingLoungeComponent,
    children: [
      {
        path:'chat/:id',
        component: LoungeChatComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NetworkingLoungeRoutingModule { }
