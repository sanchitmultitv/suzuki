import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
// import { AuthService } from '../services/auth.service';
declare var introJs: any;

@Component({
  selector: 'app-animate',
  templateUrl: './animate.component.html',
  styleUrls: ['./animate.component.scss']
})
export class AnimateComponent implements OnInit, AfterViewInit {
  uid: any;
  videoPlay = false;
  registrationDesk = false;
  intro: any;


  constructor(private router: Router) { }
  
  
    // vid.play();
  // vid.currentActiveTime = 0;
  // alert('yes')

    // this.videoplay();
    // let audioPlayer = <HTMLVideoElement>document.getElementById('loginVideo');
    // audioPlayer.addEventListener('play', () => {
    //   // audioPlayer.muted = true;
    //   audioPlayer.play();
    //   audioPlayer.muted = false;
    // });
  

  ngOnInit(): void {
    this.videoplay()
    // this.getUserguide();
    // let vid: any = document.getElementById("loginVideo");
    // vid.play();
    // this.videoplay();
    // var navigator_info = window.navigator;
    // var screen_info = window.screen;
    // this.uid = navigator_info.mimeTypes.length;
    // this.uid += navigator_info.userAgent.replace(/\D+/g, '');
    // this.uid += navigator_info.plugins.length;
    // this.uid += screen_info.height || '';
    // this.uid += screen_info.width || '';
    // this.uid += screen_info.pixelDepth || '';
    // return this.uid;
    // localStorage.setItem('virtual', JSON.stringify(this.uid));
    // const formData = new FormData();
    // var randNumber = Math.floor(Math.random() * 1000000);
    // alert(randNumber)
    // formData.append('email', this.uid + '@amazfit.com');
    // formData.append('password', 'others');
    // formData.append('mobile', '0123456789');
    // formData.append('category', 'others');
    // formData.append('company', 'others');
    // formData.append('name', 'Anonymous');

    // this.auth.loginPost(formData).subscribe((res: any) => {
    //   if (res.code === 1) {
    //     localStorage.setItem('virtual', JSON.stringify(res.result));

    //   }
    // });

  }

  ngAfterViewInit(): void {
    // alert('hi')
 
  }

  videoplay(){
  //   this.videoPlay = true;
  //         let vid: any = document.getElementById('myVideo');
  //         vid.play();
// this.receptionEnd = true;
    // this.showVideo = true;
    // let vid: any = document.getElementById("recepVideo");
    // vid.play();
  // vid.currentActiveTime = 0;
  this.registrationDesk = true;
  this.videoPlay = true;
  let vid: any = document.getElementById("loginVideo");
  // window.onload=function(){
    vid.play();
  }

  endVideo() {
    // let vid: any = document.getElementById('loginVideo');
    // vid.pause();
    // vid.currentActiveTime = 0;
    this.router.navigate(['/lobby']);
  }
  getUserguide() {
    this.intro = introJs().setOptions({
      hidePrev: true,
      hideNext: true,
      exitOnOverlayClick: false,
      exitOnEsc: false,
      showBullets: false,
      showStepNumbers: false,
      steps: [
        {
          element: document.querySelector("#reception_pulse"),
          intro: "<div style='text-align:center'>Click here to visit the lobby</div>",
          position: 'right',
        }
      ]
    }).oncomplete(() => document.cookie = "intro-complete=true");

    let start = () => this.intro.start();
    start();
    // if (document.cookie.split(";").indexOf("intro-complete=true") < 0)
    //   window.setTimeout(start, 1000);
  }

  skip() {
    let vid: any = document.getElementById('loginVideo');
    vid.pause();
    vid.currentActiveTime = 0;
    this.router.navigate(['/lobby']);
  }
}