import { Component, NgZone, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { FetchDataService } from '../services/fetch-data.service';
import { AuthService } from '../services/auth.service';
import { HttpClient } from '@angular/common/http';
declare var Razorpay:any;
declare var $:any;
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit, OnDestroy {
  checked = false;
  signupForm: FormGroup;
  items: FormArray;
  noOfEmp = 1;
  totalValue = this.noOfEmp * 11800;
  options:any =[];
  submitted = false;

  constructor(private fb:FormBuilder,private ngzone: NgZone, private http:HttpClient, private router: Router, private _fd: FetchDataService, private _auth: AuthService) { }

  ngOnInit(): void {
    this.signupForm = this.fb.group({
      company_name: ['', Validators.required],
      gstin_no : ['', Validators.required],
      company_address: ['', Validators.required],
      state_code: ['', Validators.required],
      company_email: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],
      totalValue: [this.totalValue, Validators.required],
      employees: new FormArray([this.createEmpItems()])
    });
  }

  createEmpItems(): FormGroup {
    return this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      designation: ['', Validators.required],
      phone: ['', Validators.required]
    
    });
  }

  isChecked(event){
    this.checked = event.checked;
    if(event.checked == true){
      console.log('checkin', event.checked);
      this.signupForm.patchValue({
        totalValue: this.noOfEmp * 10000
      });
    } else{
      this.signupForm.patchValue({
        totalValue: this.noOfEmp * 11800
      })
    }
  }
  response_check;
  veirfyPay:any;
  user_id;
  uname;
  uemail;
  company_id;
  child_users_id = [];
 
  createEmployee(){
    this.items = this.signupForm.get('employees') as FormArray;
    this.items.push(this.createEmpItems());
    this.noOfEmp = this.items.length;
    if(this.checked){
      this.signupForm.patchValue({
        totalValue: this.noOfEmp * 10000
      });
    } else{
      this.signupForm.patchValue({
        totalValue: this.noOfEmp * 11800
      });
    }
    console.log('asdf', this.signupForm.controls)
  }
  removeEmployee(){
    this.items = this.signupForm.get('employees') as FormArray;
    if(this.items.length>=2){
      this.items.removeAt(this.items.length-1);
      this.noOfEmp = this.items.length;
      if(this.checked){
        this.signupForm.patchValue({
          totalValue: this.noOfEmp * 10000
        });
      } else{
        this.signupForm.patchValue({
          totalValue: this.noOfEmp * 11800
        });
      } 
    }
  }
signature;
  onSubmit() {
    this.submitted = true;
    console.log('hello', this.signupForm.value.employees)

    const formData = new FormData();
    formData.append('company_address', this.signupForm.value.company_address);
    formData.append('company_email', this.signupForm.value.company_email);
    formData.append('company_name', this.signupForm.value.company_name);
    formData.append('gstin_no', this.signupForm.value.gstin_no);
    formData.append('state_code', this.signupForm.value.state_code);
    formData.append('employees', JSON.stringify(this.signupForm.value.employees));
    formData.append('total_value', this.signupForm.value.totalValue);
    this._auth.register(formData).subscribe((res: any) =>{
      console.log('hello', res);
      this.company_id = res.result.company_id;
      this.child_users_id = res.result.attendee_ids;
      let total_users = this.child_users_id.length;
      let users_list = [];
      let users_id;
      // this.child_users_id.forEach(element => {
      //   users_list.push(element+'_');
      // });
      users_id = this.child_users_id.join('_');
      const company_formData = new FormData();
      formData.append('user_id', this.company_id);
      formData.append('child_user_id', JSON.stringify(this.child_users_id));
      let sez_status;
      if(this.checked){
        sez_status = 1;
      }else{
        sez_status = 0;
      }
      if(res.code === 1){
        this.http.get('https://virtualapi.multitvsolution.com/ipf_backend/create_orders.php?user_id='+this.company_id + '&child_user_id='+users_id + '&total_users='+total_users+'&sez_status='+sez_status).subscribe((res: Response) => {
          console.log(res);
          this.response_check = res;
          if(this.response_check.status == 'success') {
            this.options = {
              'key': 'rzp_live_1K7ay6wAMdzD5y', // Enter the Key ID generated from the Dashboard
              'amount': total_users * 100 * this.signupForm.value.totalValue, // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
              'currency': 'INR',
              'name': 'IPF 2021',
              'description': 'Indian Pharmaceutical Forum 2021',
              'image': 'https://exdemo.multitvsolution.com/assets/ipf/icons/logo.png',
              'order_id': this.response_check.order_id, // This is a sample Order ID. Pass the `id` obtained in the response of Step 1
              'handler':  (response) => {
                console.log(response.razorpay_payment_id);
                console.log(response.razorpay_order_id);
                console.log(response.razorpay_signature);
                this.veirfyPay = response;
                this.verification(response);
              },
              // "modal": {
              //   "ondismiss": ()=>{
              //     // this.router.navigate(['/thankyou']);
              //     this.ngzone.runOutsideAngular(() => {
              //       this.ngzone.run(() => { 
              //         this.router.navigate(['/thankyou']);
              //        console.log('Outside Done!'); });

              //     });
              //   }
              // },
              'prefill': {
                'name':  this.uname,
                'email': this.uemail,
                  // "contact": "9999999999"
              },
            'notes': {
              'address': 'Razorpay Corporate Office'
            },
            'theme': {
              'color': '#3399cc'
            }
          };
          var rzp1 = new Razorpay(this.options);
          // document.getElementById('rzp-button1').onclick = function(e){
          rzp1.open();
          // e.preventDefault();                  
          }                
        });  
      }
    });    
  }
  verification(status:any){
    // alert('baba');
     const paymentdata = new FormData();
     paymentdata.append('razorpay_order_id', this.veirfyPay.razorpay_order_id);
     paymentdata.append('razorpay_payment_id', this.veirfyPay.razorpay_payment_id);
     paymentdata.append('razorpay_signature', this.veirfyPay.razorpay_signature);
     paymentdata.append('user_id', this.company_id);
     paymentdata.append('child_user_id', JSON.stringify(this.child_users_id));
     this.http.post('https://virtualapi.multitvsolution.com/ipf_backend/verify/verify.php', paymentdata).subscribe( 
      ((res:any) => {
        console.log('payment_verify response', res);
        if(res.status == 'success'){
         const statusData = new FormData();
        //  statusData.append('id', this.company_id);
        //  statusData.append('status', '1');
        //   // tslint:disable-next-line:align
        //   this._auth.updateStatus(statusData).subscribe(res => {
        //     console.log(res);
        //   });
 
         const mailData = new FormData();
         mailData.append('name', this.signupForm.value.company_name);
         mailData.append('email', this.signupForm.value.company_email);
         this._auth.successMail(mailData).subscribe(res => {
          //  if(status.razorpay_signature){
          //   this.router.navigate(['thankyou']);
          //   console.log(res);
          //  }
          this.ngzone.runOutsideAngular(() => {
            this.ngzone.run(() => { 
              this.router.navigate(['/thankyou']);
             console.log('Outside Done!'); 
            });
          });
          console.log(res);
          
          });
        //  $('.thanks').modal('show');
        // console.log('testing');
        this.signupForm.reset();
        this.signupForm = this.fb.group({
          company_name: ['', Validators.required],
          gstin_no : ['', Validators.required],
          company_address: ['', Validators.required],
          state_code: ['', Validators.required],
          company_email: ['', Validators.required],
          totalValue: [this.totalValue, Validators.required],
          employees: new FormArray([this.createEmpItems()])
        });
        }
   }));
  }
  ngOnDestroy(){
    this.router.navigate(['thankyou']);
  }
}
